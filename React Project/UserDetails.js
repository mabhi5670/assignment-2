import styles from './UserDetails.module.css';

const UserDetails = ({ user }) => {
    return (
        <>
            {
                user ? <section className={styles["user-details"]}>
                    <h2>{user.name}</h2>
                    <span className={styles.username}>{user.username}</span> | <span>{user.email}</span>


                    {/* ASSIGNMENT ---> POPULATE THIS DYNAMICALLY */}
                    <p>Address:
                  {user.address.suite},
                  {user.address.street},
                  {user.address.city}-
                  {user.address.zipcode}
                    </p>
                    <p>Phone:
                   {user.phone}
                    </p>

                   <p>Work Details:
                        {user.company.name}
                        ({user.company.bs})
                    </p>
                    <p>
                       {user.website}
                    </p>

                </section > : <section className={styles["user-details"]}>
                    <p>No User Selected</p>
                </section>
            }
        </>
    )
}

export default UserDetails;